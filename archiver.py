import sys
import pandas as pd
from datetime import datetime
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox

archive_file = filedialog.askopenfilename(initialdir=".", title="Select Archive File (Excel Spreadsheet)")

try:
    final_df = pd.read_excel(archive_file)
except:
    print("Error reading the archive file. You selected: %s" % archive_file)
    print("Exiting...")
    sys.exit(-1)


finished = False

while not finished:

    template_file = filedialog.askopenfilename(initialdir=".", title="Select Template File (Excel Spreadsheet)")

    try:
        template_df = pd.read_excel(template_file)
    except:
        print("Error reading the template file. You selected: %s" % template_file)
        print("Exiting...")
        sys.exit(-1)



    column_names =  ['Purpose','Name','Date leasing office called', 'Market', 'Submarket', 
                                     'Year built/Reno', '# units', '% Studio', '% 1 bedroom', '% 2 bedroom', 
                                     '% 3 bedroom', '% 4 bedroom', 'Median HH income (1mi)', 
                                     'Studio effective rent (Leasing Office)', '1 bedroom effective rent (Leasing Office)', 
                                     '2 bedroom effective rent (Leasing Office)', '3 bedroom effective rent (Leasing Office)', 
                                     '4 bedroom effective rent (Leasing Office)', 'Avg rent (Leasing Office)', 
                                     'Avg Studio rent/SF (Leasing Office)', 'Avg 1 bedroom rent/SF (Leasing Office)', 
                                     'Avg 2 bedroom rent/SF (Leasing Office)', 'Avg 3 bedroom rent/SF (Leasing Office)', 
                                     'Avg 4 bedroom rent/SF (Leasing Office)', 'Avg unit rent/SF (Leasing Office)', 
                                     'Water & Sewer Responsibility', 'Water & Sewer $ Value', 'Fuel & Gas Responsibility', 
                                     'Fuel & Gas $ Value', 'Electric Responsibility', 'Electric $ Value', 
                                     'Pest Control Responsibility', 'Pest Control $ Value', 'Trash Responsibilty Responsibility', 
                                     'Trash Responsibilty $ Value', 'Cable & Internet Responsibility', 'Cable & Internet $ Value', 
                                     'Interior Grade (1-5)', 'Exterior Grade (1-5)', 'Amenities Grade (1-5)', 
                                     'Overall Grade (1-5)', 'W/D Hookups in unit?', 'Concessions?']


    ###################
    ##TIDYING DATASET##
    ###################

    #Rotate Data
    template_df = template_df.T

    #Delete Junk 'Unnamed' Column
    template_df = template_df.reset_index().drop(['index'], axis=1)

    #Set column names to categories
    template_df = template_df.rename(columns=template_df.iloc[1]).drop([0, 1])

    #Remove duplicate columns
    template_df = template_df.loc[:, ~template_df.columns.duplicated()]

    #Add 'Purpose', 'W/D Hookups in unit?', and 'Concessions?' column (default 0)
    template_df['Purpose'] = 0
    template_df['W/D Hookups in unit?'] = 0
    template_df['Concessions?'] = 0

    #Keeping only relevant data
    template_df = template_df[column_names]

    #Rename columns to match archive
    if len(final_df.columns)==44:
        final_df = final_df.drop(['Unnamed: 0'], axis=1)

    template_df.columns = final_df.columns

    #Drop rows without a 'Property Name' value
    template_df = template_df[pd.notnull(template_df['Property Name'])]

    #Combine template data with archive
    frames = [final_df, template_df]
    final_df = pd.concat(frames, sort=False)

    #Fix weird indexing
    final_df = final_df.reset_index().drop(['index'], axis=1)
    
    msg_box = messagebox.askquestion('Continue?', 'Would you like to add more templates?')
    if msg_box=='no':
        finished=True
    
#Exporting data to file
now = datetime.now().strftime("%d-%m-%Y_%H%M%S")
print('File saved to archive_' + now + '.xls')  
final_df.to_excel("archive_" + now + ".xls")
